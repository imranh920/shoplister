﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopLister.Models
{
    public class ShopItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string DeviceId { get; set; }
        public DateTime CreateTime { get; set; }
    }
    public class DeviceName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DeviceId { get; set; }
    }
}