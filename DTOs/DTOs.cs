﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopLister.DTOs
{
    public class ShopItemDto
    {
        public string Name { get; set; }
        public double Price { get; set; }
        HttpPostedFileBase Image1 { get; set; }
        HttpPostedFileBase Image2 { get; set; }
        HttpPostedFileBase Image3 { get; set; }
        public string DeviceId { get; set; }
    }
    public class DtoSell
    {
        public double Price { get; set; }
        public string DeviceId { get; set; }
    }
    public class DtoSellItemsResultContainer
    {
        public List<DtoSell> items { get; set; }
        public double price { get; set; }
    }
}