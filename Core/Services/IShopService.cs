﻿using ShopLister.DTOs;
using ShopLister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopLister.Core.Services
{
    public interface IShopService
    {
        void SaveShopItem(ShopItem item, HttpFileCollectionBase files);
        List<ShopItem> GetShopItemList(DateTime? fromDate, DateTime? toDate, DateTime? fromTime, DateTime? toTime);
        DtoSellItemsResultContainer GetIndividualSaleList(DateTime? fromDate, DateTime? toDate);
        List<DeviceName> GetDeviceList();
        void SaveName(DeviceName model);
    }
}