﻿using Dapper;
using ShopLister.DTOs;
using ShopLister.Models;
using ShopLister.Repository;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace ShopLister.Core.Services
{
    public class ShopService : IShopService
    {
        IShopListRepository _shopListRepo;
        public ShopService()
        {
            _shopListRepo = new ShopListRepository();
        }
        public void SaveShopItem(ShopItem item, HttpFileCollectionBase files)
        {
            int i = 0;
            foreach (string file in files)
            {
                var postedFile = files[file];
                string relativePath = "/Contents/" + item.Name;
                var folderPath = HttpContext.Current.Server.MapPath("~" + relativePath);
                System.IO.Directory.CreateDirectory(folderPath);
                var filePath = folderPath + "/" + postedFile.FileName;
                var img = Image.FromStream(postedFile.InputStream);
                ExifRotate(ref img);
                img.Save(filePath);
                if (i == 0)
                    item.Image1 = relativePath + "/" + postedFile.FileName;
                else if(i == 1)
                    item.Image2 = relativePath + "/" + postedFile.FileName;
                else
                    item.Image3 = relativePath + "/" + postedFile.FileName;
                i++;
                // NOTE: To store in memory use postedFile.InputStream
            }
            item.CreateTime = DateTime.Now;
            _shopListRepo.SaveShopItem(item);
        }
        private const int exifOrientationID = 0x112; //274

        public void ExifRotate(ref Image img)
        {
            if (!img.PropertyIdList.Contains(exifOrientationID))
                return;

            var prop = img.GetPropertyItem(exifOrientationID);
            int val = BitConverter.ToUInt16(prop.Value, 0);
            var rot = RotateFlipType.RotateNoneFlipNone;

            if (val == 3 || val == 4)
                rot = RotateFlipType.Rotate180FlipNone;
            else if (val == 5 || val == 6)
                rot = RotateFlipType.Rotate90FlipNone;
            else if (val == 7 || val == 8)
                rot = RotateFlipType.Rotate270FlipNone;

            if (val == 2 || val == 4 || val == 5 || val == 7)
                rot |= RotateFlipType.RotateNoneFlipX;

            if (rot != RotateFlipType.RotateNoneFlipNone)
                img.RotateFlip(rot);
        }
        public List<ShopItem> GetShopItemList(DateTime? fromDate, DateTime? toDate, DateTime? fromTime, DateTime? toTime)
        {
            var startTime = fromTime.HasValue? fromTime.Value : new DateTime(year: 2020, month: 1, day: 1, hour: 0, minute: 0, second: 0);
            var endTime = toTime.HasValue ? toTime.Value : new DateTime(year: 2020, month: 1, day: 1, hour: 23, minute: 0, second: 0);
            int i = 0;
            var from = fromDate.HasValue ? fromDate.Value.Date : DateTime.Now.Date;
            from = from.AddHours(startTime.Hour).AddMinutes(startTime.Minute);
            var to = from.AddHours(endTime.Hour).AddMinutes(endTime.Minute);

            string sql = string.Format($"select si.id, si.price, si.image1, si.image2, si.image3, dn.name as deviceid, si.createtime " +
                "from shopitems as si left join devicenames as dn on si.deviceid = dn.deviceid where CreateTime >= @createTime and CreateTime <= @endTime");
            var items = _shopListRepo.GetConn().Query<ShopItem>(sql, new { createTime = from, endTime = to }).OrderBy(s => s.DeviceId).ToList();
            return items;
        }
        public DtoSellItemsResultContainer GetIndividualSaleList(DateTime? fromDate, DateTime? toDate)
        {
            int i = 0;
            var from = fromDate.HasValue ? fromDate.Value.Date : DateTime.Now.Date.AddDays(-1);
            var to = toDate.HasValue ? toDate.Value.Date : from.AddDays(1);

            string sql = string.Format($"select si.id, dn.name as deviceid, price from shopitems as si " +
                            $"left join devicenames as dn on si.deviceid = dn.deviceid " +
                            $"where CreateTime >= @createTime and CreateTime <= @endTime");
            var items = _shopListRepo.GetConn().Query<DtoSell>(sql, new { createTime = from, endTime = to }).OrderBy(s => s.DeviceId).ToList();

            var finalItems = items.GroupBy(s => s.DeviceId).Select(s => new DtoSell { DeviceId = s.Key, Price = s.Select(x => x.Price).Sum() }).ToList();
            return new DtoSellItemsResultContainer { items = finalItems, price = finalItems.Select(s => s.Price).Sum() };
        }
        public List<DeviceName> GetDeviceList()
        {
            string sql = string.Format($"select distinct deviceid from shopitems");
            var devIds = _shopListRepo.GetConn().Query<string>(sql).ToList();

            List<DeviceName> items = new List<DeviceName>();
            
            sql = string.Format($"select deviceid from devicenames");
            var devIds2 = _shopListRepo.GetConn().Query<string>(sql).ToList();

            var newIds = devIds.Where(s => !devIds2.Contains(s)).ToList();

            if(newIds.Count > 0)
            {
                newIds.ForEach(s =>
                {
                    _shopListRepo.DeviceName(new DeviceName { DeviceId = s, Name = "" });
                });
            }
            sql = string.Format($"select * from devicenames");
            items = _shopListRepo.GetConn().Query<DeviceName>(sql).ToList();
            return items;
        }
        public void SaveName(DeviceName model)
        {
            string sql = string.Format($"update devicenames set [name] = '" + model.Name +"' where [id] = " + model.Id);
            _shopListRepo.GetConn().Execute(sql);
        }
    }
}