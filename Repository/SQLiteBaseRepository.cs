﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;

namespace ShopLister.Repository
{
    public class SqLiteBaseRepository
    {
        public SqLiteBaseRepository()
        {
            if (!File.Exists(DbFile))
                CreateDatabase();
        }
        public static string DbFile
        {
            get { return System.Web.Hosting.HostingEnvironment.MapPath("~/sale.sqlite"); }
        }

        public static SQLiteConnection SimpleDbConnection()
        {
            return new SQLiteConnection("Data Source=" + DbFile, true);
        }
        private static void CreateDatabase()
        {
            using (var cnn = SimpleDbConnection())
            {
                cnn.Open();
                cnn.Execute(
                    @"create table ShopItems
                    (
                        ID                  INTEGER PRIMARY KEY AUTOINCREMENT,
                        Name                varchar(100) null,
                        Price               double(8) not null,
                        Image1              varchar(300) null,
                        Image2              varchar(300) null,
                        Image3              varchar(300) null,
                        DeviceId            varchar(100) null,
                        CreateTime          datetime not null
                    )"
                );
                cnn.Execute(
                    @"create table DeviceNames
                    (
                        ID                  INTEGER PRIMARY KEY AUTOINCREMENT,
                        Name                varchar(100) null,
                        DeviceId            varchar(100) null
                    )"
                );
            }
        }
    }
}