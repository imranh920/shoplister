﻿using ShopLister.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Web;

namespace ShopLister.Repository
{
    public interface IShopListRepository
    {
        ShopItem GetCustomer(long id);
        void SaveShopItem(ShopItem item);
        void DeviceName(DeviceName item);
        IEnumerable<T> GetByQuery<T>(string query);
        SQLiteConnection GetConn();
    }

}