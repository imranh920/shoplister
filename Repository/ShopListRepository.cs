﻿using Dapper;
using Dapper.Contrib.Extensions;
using ShopLister.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;

namespace ShopLister.Repository
{
    public class ShopListRepository : SqLiteBaseRepository, IShopListRepository, IDisposable
    {
        SQLiteConnection _repo = null;
        public ShopListRepository() : base()
        {
            _repo = SimpleDbConnection();
            _repo.Open();
        }
        public ShopItem GetCustomer(long id)
        {
            throw new NotImplementedException();
        }

        public void SaveShopItem(ShopItem item)
        {
            if (!File.Exists(DbFile));

            using (var cnn = SimpleDbConnection())
            {
                cnn.Open();
                cnn.Insert<ShopItem>(item);   
            }
        }
        public void DeviceName(DeviceName item)
        {
            if (!File.Exists(DbFile)) ;

            using (var cnn = SimpleDbConnection())
            {
                cnn.Open();
                cnn.Insert<DeviceName>(item);
            }
        }
        public IEnumerable<T> GetByQuery<T>(string query)
        {
            List<T> resp = _repo.Query<T>(query).ToList();
            return resp;
        }

        public SQLiteConnection GetConn()
        {
            return _repo;
        }

        public void Dispose()
        {
            _repo?.Close();
            _repo?.Dispose();
        }
    }
}