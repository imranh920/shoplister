﻿using ShopLister.Core.Services;
using ShopLister.DTOs;
using ShopLister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace ShopLister.Controllers
{
    public class HomeController : Controller
    {
        IShopService _shopService;
        public HomeController()
        {
            _shopService = new ShopService();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SaveShopItem(ShopItem item)
        {
            var req =this.Request;
            if(req.Files.Count < 1)
                return Json(new
                {
                    value = "Need at least 1 file"
                });
            _shopService.SaveShopItem(item, req.Files);
            return Json(new { 
                value = "Ok"
            });
        }
        public ActionResult GetAllItems(DateTime? fromDate = null, DateTime? fromTime = null, DateTime? toTime = null)
        {
            if (!fromDate.HasValue)
                fromDate = DateTime.Now.Date;

            if (!fromTime.HasValue)
                fromTime = new DateTime(year: 2020, month: 1, day: 1, hour: 7, minute: 0, second: 0);

            if (!toTime.HasValue)
                toTime = new DateTime(year: 2020, month: 1, day: 1, hour: 23, minute: 0, second: 0);

            ViewBag.fromTime = fromTime;
            ViewBag.toTime = toTime;
            ViewBag.fromDate = fromDate;
            

            return View( "Report", _shopService.GetShopItemList(fromDate: fromDate, toDate: null, fromTime: fromTime, toTime: toTime));
        }
        public ActionResult GetAllIndividualReport(DateTime? fromDate = null, DateTime? toDate = null)
        {
            if (!fromDate.HasValue)
                fromDate = DateTime.Now.Date.AddDays(-1);

            if (!toDate.HasValue)
                toDate = DateTime.Now.Date;

            ViewBag.fromDate = fromDate;
            ViewBag.toDate = toDate;

            return View("IndividualReport", _shopService.GetIndividualSaleList(fromDate: fromDate, toDate: toDate));
        }
        public ActionResult GetDeviceList()
        {
            return View("DeviceNames", _shopService.GetDeviceList());
        }
        [HttpPost]
        public ActionResult SaveName(DeviceName model)
        {
            _shopService.SaveName(model);
            return RedirectToAction("GetDeviceList");
        }
    }
}
